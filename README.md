# Antibiotic networks team

**Present at the *VII congreso de biouímica y biología molecular de bacterias***

![](figs/logo.png)

[Bioinformatics & complex networks lab](https://ira.cinvestav.mx/ingenieriagenetica/dra-maribel-hernandez-rosales/bioinformatica-y-redes-complejas/)

[Molecular biology & microbial ecology lab](https://ira.cinvestav.mx/ingenieriagenetica/dra-gabriela-olmedo-alvarez/laboratorio-de-biologia-molecular-y-ecologia-microbiana/)

---

- **Poster 20**: Reconstruction of the evolutionary history of antibiotic-resistant genes in ancient and
  modern bacterial communities using REvolutionH-tl.

  Ramirez-Rafael José Antonio 

- **Poster 28**: Antibiotic Resistance Genes in microbial isolates from Cuatrociénegas, Coahuila.

  Barceló-Antemate Diana

- **Poster 134**: The Pangenome Puzzle: Decoding Ecological Roles and Taxonomic Affiliation in Microbial Community Competitive Interactions

![](figs/figs.png)
